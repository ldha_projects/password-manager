import sys
from PyQt5 import QtWidgets 
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QMessageBox
import json
from tools.ui_main import *
import json
from random import randint
from tools.cript import *
import pandas as pd

class PasswordManager(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self)
        self.ui = Ui_PasswordManager()
        self.ui.setupUi(self)

        # Buttons
        self.ui.add_btn.clicked.connect(self.addToFile)
        self.ui.search_btn.clicked.connect(self.searchAccount)
        self.ui.gen_btn.clicked.connect(self.fillRandomPassword)
        
        # Class variables
        self.pages = {}
        self.getData()

    # Gets the data from the json to stay up to date
    def getData(self):
        try:
            with open("/home/ne0ri0/Documents/MyInfo.json", "r") as fi:
                self.pages = json.loads(fi.read())            
        except:
            self.pages = {}
    
    #window Search Account
    def searchAccount(self):
        ktc = [[comparable(xkey),xkey] for xkey in self.pages]
        wtc = comparable(encrypt(self.ui.url_search.text()))
        for keyList in ktc:
            if wtc in keyList[0]:              
                myKey = keyList[1]
                self.ui.user_search.setText(decrypt(self.pages[myKey]['usr']))
                self.ui.psw_search.setText(decrypt(self.pages[myKey]['psw']))
                df=pd.DataFrame([self.ui.psw_search.text()])
                df.to_clipboard(index=False,header=False)
                self.getData()
                QMessageBox.warning(self, 'Clipboard:', 'Copied password to clipboard')
                break

            else:
                self.ui.user_search.setText("")
                self.ui.psw_search.setText("")


    # Window Save New Account
    # Checks if you already have an account with such name, then saves it 
    def addToFile(self):
        if self.ui.url_edit.text() == "" or self.ui.user_edit.text() == "" or self.ui.psw_edit.text() == "" or self.ui.ver_edit.text() == "" :
            QMessageBox.warning(self, 'Required Data:', 'Something is missing')
        else:
            if self.ui.psw_edit.text() == self.ui.ver_edit.text():
                if comparable(encrypt(self.ui.url_edit.text())) in [comparable(xkey) for xkey in self.pages]: 
                    #################
                    ktc = [[comparable(xkey),xkey] for xkey in self.pages]
                    wtc = comparable(encrypt(self.ui.url_edit.text()))
                    for keyList in ktc:
                        if wtc in keyList[0]:              
                            myKey = keyList[1]
                        ##########
                            reply = QMessageBox.question(
                                self, 'Account Overwrite:', 'You already have an account for that web page. \n do you want to update it?', QMessageBox.Yes | QMessageBox.No)
                            if reply == QMessageBox.Yes:
                                self.pages[myKey] = {"url":encrypt(self.ui.url_edit.text()), "usr":encrypt(self.ui.user_edit.text()), "psw":encrypt(self.ui.psw_edit.text())}
                                line = json.dumps(self.pages, indent=2)
                                with open("/home/ne0ri0/Documents/MyInfo.json", "w") as f:
                                    f.write(line)
                                df=pd.DataFrame([self.ui.psw_edit.text()])
                                df.to_clipboard(index=False,header=False)
                                self.getData()
                                QMessageBox.warning(self, 'Clipboard:', 'Copied password to clipboard')
                            else:
                                pass
                else:
                    self.pages[encrypt(self.ui.url_edit.text())] = {"url":encrypt(self.ui.url_edit.text()), "usr":encrypt(self.ui.user_edit.text()), "psw":encrypt(self.ui.psw_edit.text())}
                    line = json.dumps(self.pages, indent=2)
                    with open("/home/ne0ri0/Documents/MyInfo.json", "w") as f:
                        f.write(line)
                    df=pd.DataFrame([self.ui.psw_edit.text()])
                    df.to_clipboard(index=False,header=False)
                    self.getData()
                    QMessageBox.warning(self, 'Clipboard:', 'Copied password to clipboard')
            else:
                QMessageBox.warning(self, 'Password Error:', 'The passwords fields do not match')

    # Displays random generated passwords on password line edit and in ver line edit
    def fillRandomPassword(self):
        psw_generated = randomPassword()
        self.ui.psw_edit.setText(psw_generated)
        self.ui.ver_edit.setText(psw_generated)

    # Close Program
    def closeEvent(self, event):          
        reply = QMessageBox.question(self, 'Close application', 'Are you sure you want to close the window?', QMessageBox.Yes | QMessageBox.No)
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()
       

def main_window():  # Run application
    app = QApplication(sys.argv)
    # create and show mainWindow
    mainWindow = PasswordManager()
    mainWindow.showMaximized()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main_window()