import random
from random import randint


values = {'A': '1', 'B': '2', 'C': '3', 'D': '4', 'E': '5', 'F': '6', 'G': '7', 'H': '8', 'I': '9', 'J': '10', 'K': '11', 'L': '12', 'M': '13', 'N': '14','Ñ':'15', 'O': '16', 'P': '17', 'Q': '18', 'R': '19', 'S': '20', 'T': '21', 'U': '22', 'V': '23', 'W': '24', 'X': '25', 'Y': '26', 'Z': '27','a': '28', 'b': '29', 'c': '30', 'd': '31', 'e': '32', 'f': '33', 'g': '34', 'h': '35', 'i': '36', 'j': '37', 'k': '38', 'l': '39', 'm': '40', 'n': '41', 'ñ':'42', 'o': '43', 'p': '44', 'q': '45', 'r': '46', 's': '47', 't': '48', 'u': '49', 'v': '50', 'w': '51', 'x': '52', 'y': '53', 'z': '54', '0':'55', '1':'56', '2':'57','3': '58','4': '59','5': '60','6': '61','7': '62','8': '63','9': '64', '[':'65',']':'66','!':'67','@':'68','?':'69', '.': '70', ',':'71'}
nvalues= {'1':'1','2':'!','3':'@','4':'#','5':'2','6':'$','7':'^','8':'*','9':'3','10':'(','11':')','12':'-','13':'_','14':'=','15':'+','16':'4','17':'[','18':'{','19':']','20':'}','21':';','22':'5','23':':','24':'~','25':',','26':'<','27':'.','28':'6','29':'>','30':'/','31':'?','32':'7','33':'q','34':'w','35':'e','36':'8','37':'r','38':'t','39':'y','40':'u','41':'i','42':'i','43':'9','44':'o','45':'p','46':'a','47':'s','48':'d','49':'0','50':'f','51':'g','52':'h','53':'j','54':'k','55':'l','56':'z','57':'x','58':'c','59':'v','60':'b','61':'n','62':'m','63':'A','64':'Z', '65':'B','66':'C','67':'D','68':'E','69':'F','70':'G','71':'H'}

def getKey(dictOfElements, valueToFind):
    key = None
    listOfItems = dictOfElements.items()
    for item  in listOfItems:
        if item[1] == valueToFind:
            key = item[0]
            break
    return  key

def comparable(word):
    a,b = int(word[0])+1, int(word[-1])+1
    c=list(word)
    del c[:a]
    del c[-b:]
    word=''
    for d in c: word += d
    return word

def decrypt(word):
    decripted = ""
    to_decript = comparable(word)
    for character in to_decript:
        decripted += getKey(values,getKey(nvalues, character))
    return decripted

def encrypt(word):
    #---------------------------------------------------
    randstart = random.choice(range(2,9))
    randend = random.choice(range(2,9))

    li = []
    li2=[]
    lis = []
    lis2 = []
    for ran in range(1,randstart):
        li.append(str(random.choice(range(1,64))))
    for ran in range(1,randend):
        lis.append(str(random.choice(range(1,64))))
    for i in li:
        li2.append(nvalues[i])
    for i in lis:
        lis2.append(nvalues[i])
    #---------------------------------------------------
    list1=list(word)
    list2=[]
    list3 = [str(randstart-1)]
    for l in li2: list3.append(l)
    encripted = ''
    for i in list1:
        list2.append(values[i])        
    for n in list2:
        list3.append(nvalues[n])
    for l in lis2: list3.append(l)
    list3.append(str(randend-1))
    for x in list3:
        encripted += x
    return encripted

def randomPassword():
    mayus = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8','9','0','[',']','!','@','?']
    r_password = ""
    for _ in range(randint(15,25)):
        r_password += mayus[randint(0,len(mayus)-1)]
    return r_password

if __name__ == "__main__":
    from time import sleep
    
    # for _ in range(20):
    #     test_x = randomPassword()
    #     crip_x = encrypt(test_x)
    #     print(crip_x)
    #     print(comparable(crip_x))
    #     dec_x=decrypt(crip_x)
    #     if test_x == dec_x:
    #         print("success!!!!!")
    #     else:
    #         print('fail :(((')
    #         break

    test_x =getKey(nvalues, 'H')
    print(test_x)
